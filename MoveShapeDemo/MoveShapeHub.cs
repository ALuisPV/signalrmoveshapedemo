﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using System.Threading;

namespace MoveShapeDemo
{
    // Clase Broadcaster: Sera la encargada de gestionar el envio de mensajes de los clientes conectados
    public class Broadcaster
    {
        // Instancia de la Clase Broadcaster. Inicializamos de forma diferida. Propiedad Estatica
        private readonly static Lazy<Broadcaster> _instance = new Lazy<Broadcaster>(() => new Broadcaster());
        // Timer para enviar mensajes a los clientes. Cada 40 ms. Maximo: 25 veces por segundo
        private readonly TimeSpan BroadcastInterval = TimeSpan.FromMilliseconds(40);
        // Interfaz IHubContext - Lo usamos para tener acceso al Contexto del HUB
        private readonly IHubContext _hubContext;
        // Timer para el Loop del Servidor
        private Timer _broadcastLoop;
        // Datos del Modelo: Posiciones y ultima actualizacion
        private ShapeModel _model;
        // Flag que indica si el modelo de ha actualizado
        private bool _modelUpdated;
        // Constructor
        public Broadcaster()
        {
            // Salvar el Contexto del HUB MoveShapeHub. Podremos usarlo facilmente para enviar datos a clientes
            _hubContext = GlobalHost.ConnectionManager.GetHubContext<MoveShapeHub>();
            // Datos de Modelo. Inicializacion
            _model = new ShapeModel();
            // Flag de Actualizacion. Por defecto, false
            _modelUpdated = false;
            // Iniciar el BUCLE de Broadcast
            _broadcastLoop = new Timer(
                BroadcastShape,
                null,
                BroadcastInterval,
                BroadcastInterval);
        }

        // Método que ejecuta el Loop. Actualiza a los clientes si se ha actualizado el Modelo
        public void BroadcastShape(object state)
        {
            // Si el modelo se actualiza, se ejecuta la actualizacion. Llamamos a los clientes, excepto al que genera la llamada
            if (_modelUpdated)
            {
                // Actualizar a los clientes, a traves del Contexto del HUB. Todos excepto el que envia la peticion
                _hubContext.Clients.AllExcept(_model.LastUpdatedBy).updateShape(_model);
                // Figura Movida. Indicamso que ha quedado movida y quitamso el flag de movimiento
                _modelUpdated = false;
            }
        }

        // Actualizar la posicion de la Figura. Esto es lo que se llama cuando los clientes actualizan.
        // NO ENVIA inmediatamente la info del resto de clientes. Solo actualiza. 
        // La Info la gestiona el Broadcaster mediante el Timer
        public void UpdateShape(ShapeModel clientModel)
        {
            // Actualizar Modelo
            _model = clientModel;
            // Marcarlo como actualizado
            _modelUpdated = true;
        }

        // Propiedad que devuelve la Instancia concreta de Broadcaster creada de forma "Lazy"
        public static Broadcaster Instance
        {
            get
            {
                return _instance.Value;
            }
        }
    }

    public class MoveShapeHub : Hub
    {
        // Enlace a nuestro Broadcaster
        private Broadcaster _broadcaster;

        // Constructor - Inicializar la Propiedad _broadcaster con la Instancia que se crea.
        // OJO: El Broadcaster se crea "En Modo Lazy"
        public MoveShapeHub()
            : this(Broadcaster.Instance)
        {
        }

        // Constructor - Si nos pasan un Broadcaster ya creado, sera el que se use
        public MoveShapeHub(Broadcaster broadcaster)
        {
            // Establecer la propiedad _broadcaster al que nos pasan en este Constructor
            _broadcaster = broadcaster;
        }

        // Actualizar el Modelo. Nueva posicion actualizar en nuestro Broadcaster. Esto es lo que se llamada desde el HUB en si. 
        // Cuando los clientes llamen el Servidor, ESTO es lo que se ejecuta. Se actualizará cuando el Broadcaster asi lo decida
        // La actualizacion de clientes queda gestionada por el Timer
        public void UpdateModel(ShapeModel clientModel)
        {
            // Actualizar quien ha modificado por ultima vez
            clientModel.LastUpdatedBy = Context.ConnectionId;
            // Actualizar el Modelo
            _broadcaster.UpdateShape(clientModel);
        }

    }

    // Para almacenar los datos de la Figura
    // Al cliente solo se el envia las propiedades top y left, anotadas con JsonProperty
    // La propiedad de LastUpdatedBy no se envia. Se anota con JSonIgnore
    public class ShapeModel
    {
        // Declaramos Left y Top con minusculas, aunque a nivel de propiedades de app, la primera letra sea mayuscula
        [JsonProperty("left")]
        public double Left { get; set; }
        [JsonProperty("top")]
        public double Top { get; set; }
        // No queremos que los clientes tengan el dato "LastUpdatedBy". Esta propiedad se anota para que no se envie
        [JsonIgnore]
        public string LastUpdatedBy { get; set; }
    }
}
